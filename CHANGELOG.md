# Changelog

## Unreleased

### Added

- Installable en tant que package Composer

### Changed

- Compatible SPIP 5.0.0-dev

### Security

- spip-team/securite#4841 Limiter l’usage de `#ENV**` dans les formulaires.
